/*
SQLyog Ultimate v11.33 (64 bit)
MySQL - 5.7.27-0ubuntu0.18.04.1 : Database - hotel_century
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`hotel_century` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `hotel_century`;

/*Table structure for table `barang_detail` */

DROP TABLE IF EXISTS `barang_detail`;

CREATE TABLE `barang_detail` (
  `id_barang_detail` int(11) NOT NULL AUTO_INCREMENT,
  `nama_barang` varchar(50) NOT NULL,
  `jumlah_barang` int(100) NOT NULL,
  `id_barang_header` int(11) NOT NULL,
  PRIMARY KEY (`id_barang_detail`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `barang_detail` */

/*Table structure for table `barang_header` */

DROP TABLE IF EXISTS `barang_header`;

CREATE TABLE `barang_header` (
  `id_barang_header` int(11) NOT NULL AUTO_INCREMENT,
  `id_customer` int(11) NOT NULL,
  `tanggal` date NOT NULL,
  PRIMARY KEY (`id_barang_header`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `barang_header` */

/*Table structure for table `bellboy_master` */

DROP TABLE IF EXISTS `bellboy_master`;

CREATE TABLE `bellboy_master` (
  `id_bellboy` int(11) NOT NULL AUTO_INCREMENT,
  `nama_bellboy` varchar(50) NOT NULL,
  `nip` int(20) NOT NULL,
  PRIMARY KEY (`id_bellboy`)
) ENGINE=InnoDB AUTO_INCREMENT=11443 DEFAULT CHARSET=latin1;

/*Data for the table `bellboy_master` */

insert  into `bellboy_master`(`id_bellboy`,`nama_bellboy`,`nip`) values (1234,'Iwan',123),(1235,'Taupik',125),(11441,'Supriyatno',10451234),(11442,'Fajar',114171234);

/*Table structure for table `customer` */

DROP TABLE IF EXISTS `customer`;

CREATE TABLE `customer` (
  `id_customer` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(100) NOT NULL,
  `no_ktp` varchar(30) NOT NULL,
  `alamat` text NOT NULL,
  `no_hp` int(11) NOT NULL,
  PRIMARY KEY (`id_customer`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `customer` */

/*Table structure for table `master_jabatan` */

DROP TABLE IF EXISTS `master_jabatan`;

CREATE TABLE `master_jabatan` (
  `id_jabatan` int(11) NOT NULL AUTO_INCREMENT,
  `nama_jabatan` varchar(20) NOT NULL,
  `status_jabatan` tinyint(1) NOT NULL,
  PRIMARY KEY (`id_jabatan`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `master_jabatan` */

/*Table structure for table `master_kamar` */

DROP TABLE IF EXISTS `master_kamar`;

CREATE TABLE `master_kamar` (
  `id_kamar` int(11) NOT NULL AUTO_INCREMENT,
  `kode_kamar` varchar(20) NOT NULL,
  `type_kamar` varchar(20) NOT NULL,
  PRIMARY KEY (`id_kamar`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

/*Data for the table `master_kamar` */

insert  into `master_kamar`(`id_kamar`,`kode_kamar`,`type_kamar`) values (1,'001','Deluxe'),(2,'006','President Suite'),(3,'004','Junior Suit'),(4,'005','Century Suit'),(5,'002','executive'),(6,'003','Premium');

/*Table structure for table `master_pegawai` */

DROP TABLE IF EXISTS `master_pegawai`;

CREATE TABLE `master_pegawai` (
  `id_pegawai` int(11) NOT NULL AUTO_INCREMENT,
  `nip_pegawai` varchar(20) NOT NULL,
  `nama_pegawai` varchar(100) NOT NULL,
  `alamat_pegawai` varchar(100) NOT NULL,
  `email_pegawai` varchar(50) NOT NULL,
  `password_pegawai` varchar(20) NOT NULL,
  `id_jabatan` int(1) NOT NULL,
  PRIMARY KEY (`id_pegawai`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `master_pegawai` */

/*Table structure for table `migration` */

DROP TABLE IF EXISTS `migration`;

CREATE TABLE `migration` (
  `version` varchar(180) NOT NULL,
  `apply_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `migration` */

insert  into `migration`(`version`,`apply_time`) values ('m000000_000000_base',1581350592),('m130524_201442_init',1581350594),('m190124_110200_add_verification_token_column_to_user_table',1581350594);

/*Table structure for table `pengantar_barang` */

DROP TABLE IF EXISTS `pengantar_barang`;

CREATE TABLE `pengantar_barang` (
  `id_pengantar` int(11) NOT NULL AUTO_INCREMENT,
  `id_barang_header` int(11) NOT NULL,
  `id_bellboy` int(11) NOT NULL,
  `jam_antar` datetime NOT NULL,
  `jam_selesai` datetime NOT NULL,
  PRIMARY KEY (`id_pengantar`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `pengantar_barang` */

/*Table structure for table `penilaian` */

DROP TABLE IF EXISTS `penilaian`;

CREATE TABLE `penilaian` (
  `id_penilaian` int(11) NOT NULL AUTO_INCREMENT,
  `id_customer` int(11) NOT NULL,
  `komentar` text NOT NULL,
  PRIMARY KEY (`id_penilaian`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

/*Data for the table `penilaian` */

insert  into `penilaian`(`id_penilaian`,`id_customer`,`komentar`) values (1,0,'Aduh susah nih customernya');

/*Table structure for table `transaksi_kamar_detail` */

DROP TABLE IF EXISTS `transaksi_kamar_detail`;

CREATE TABLE `transaksi_kamar_detail` (
  `id_transaksi_kamar_detail` int(11) NOT NULL AUTO_INCREMENT,
  `id_transaksi_kamar_header` int(11) NOT NULL,
  `id_kamar` int(11) NOT NULL,
  PRIMARY KEY (`id_transaksi_kamar_detail`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `transaksi_kamar_detail` */

/*Table structure for table `trx_bellboy` */

DROP TABLE IF EXISTS `trx_bellboy`;

CREATE TABLE `trx_bellboy` (
  `id_trx_bellboy` int(11) NOT NULL AUTO_INCREMENT,
  `id_pegawai` int(11) NOT NULL,
  `tgl_trx` date NOT NULL,
  `status` int(1) NOT NULL,
  `tgl_trx_mulai` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `tgl_trx_selesai` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id_trx_bellboy`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `trx_bellboy` */

/*Table structure for table `trx_tamu` */

DROP TABLE IF EXISTS `trx_tamu`;

CREATE TABLE `trx_tamu` (
  `id_transaksi_kamar_header` int(11) NOT NULL AUTO_INCREMENT,
  `id_kamar` int(11) NOT NULL,
  `id_customer` int(11) NOT NULL,
  `kode_pesan` varchar(20) NOT NULL,
  `tanggal_checkin` datetime NOT NULL,
  `tanggal_checkout` datetime NOT NULL,
  PRIMARY KEY (`id_transaksi_kamar_header`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `trx_tamu` */

/*Table structure for table `user` */

DROP TABLE IF EXISTS `user`;

CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `auth_key` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `password_hash` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password_reset_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` smallint(6) NOT NULL DEFAULT '10',
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL,
  `verification_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`),
  UNIQUE KEY `email` (`email`),
  UNIQUE KEY `password_reset_token` (`password_reset_token`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `user` */

insert  into `user`(`id`,`username`,`auth_key`,`password_hash`,`password_reset_token`,`email`,`status`,`created_at`,`updated_at`,`verification_token`) values (1,'admin','7gP3KF6zoC9tayfrRme9cY06KaxhvpN4','$2y$13$zoq/ZpahYSUK2qefVVnzXuBArTiSwUN4Q3vsluztNY5kZ1LNYo5r6',NULL,'admin@gmil.com',10,1581350650,1581350650,'5I-tTYWB_K5Lxrgv2lVG8Ct7UeW15W70_1581350650');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
