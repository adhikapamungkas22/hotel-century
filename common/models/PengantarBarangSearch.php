<?php

namespace common\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\PengantarBarang;

/**
 * PengantarBarangSearch represents the model behind the search form of `common\models\PengantarBarang`.
 */
class PengantarBarangSearch extends PengantarBarang
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_pengantar', 'id_barang_header', 'id_bellboy'], 'integer'],
            [['jam_antar', 'jam_selesai'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = PengantarBarang::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id_pengantar' => $this->id_pengantar,
            'id_barang_header' => $this->id_barang_header,
            'id_bellboy' => $this->id_bellboy,
            'jam_antar' => $this->jam_antar,
            'jam_selesai' => $this->jam_selesai,
        ]);

        return $dataProvider;
    }
}
