<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "master_jabatan".
 *
 * @property int $id_jabatan
 * @property string $nama_jabatan
 * @property int $status_jabatan
 */
class MasterJabatan extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'master_jabatan';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nama_jabatan', 'status_jabatan'], 'required'],
            [['status_jabatan'], 'integer'],
            [['nama_jabatan'], 'string', 'max' => 20],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_jabatan' => 'Id Jabatan',
            'nama_jabatan' => 'Nama Jabatan',
            'status_jabatan' => 'Status Jabatan',
        ];
    }
}
