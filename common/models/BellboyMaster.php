<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "bellboy_master".
 *
 * @property int $id_bellboy
 * @property string $nama_bellboy
 * @property int $nip
 */
class BellboyMaster extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'bellboy_master';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nama_bellboy', 'nip'], 'required'],
            [['nip'], 'integer'],
            [['nama_bellboy'], 'string', 'max' => 50],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_bellboy' => 'Id Bellboy',
            'nama_bellboy' => 'Nama Bellboy',
            'nip' => 'Nip',
        ];
    }
}
