<?php

namespace common\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\TrxBellboy;

/**
 * TrxBellboySearch represents the model behind the search form of `common\models\TrxBellboy`.
 */
class TrxBellboySearch extends TrxBellboy
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_trx_bellboy', 'id_pegawai', 'status'], 'integer'],
            [['tgl_trx', 'tgl_trx_mulai', 'tgl_trx_selesai'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = TrxBellboy::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id_trx_bellboy' => $this->id_trx_bellboy,
            'id_pegawai' => $this->id_pegawai,
            'tgl_trx' => $this->tgl_trx,
            'status' => $this->status,
            'tgl_trx_mulai' => $this->tgl_trx_mulai,
            'tgl_trx_selesai' => $this->tgl_trx_selesai,
        ]);

        return $dataProvider;
    }
}
