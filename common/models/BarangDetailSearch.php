<?php

namespace common\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\BarangDetail;

/**
 * BarangDetailSearch represents the model behind the search form of `common\models\BarangDetail`.
 */
class BarangDetailSearch extends BarangDetail
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_barang_detail', 'jumlah_barang', 'id_barang_header'], 'integer'],
            [['nama_barang'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = BarangDetail::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id_barang_detail' => $this->id_barang_detail,
            'jumlah_barang' => $this->jumlah_barang,
            'id_barang-header' => $this->id_barang_header,
        ]);

        $query->andFilterWhere(['like', 'nama_barang', $this->nama_barang]);

        return $dataProvider;
    }
}
