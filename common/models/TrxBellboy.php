<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "trx_bellboy".
 *
 * @property int $id_trx_bellboy
 * @property int $id_pegawai
 * @property string $tgl_trx
 * @property int $status
 * @property string $tgl_trx_mulai
 * @property string $tgl_trx_selesai
 */
class TrxBellboy extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'trx_bellboy';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_pegawai', 'tgl_trx', 'status'], 'required'],
            [['id_pegawai', 'status'], 'integer'],
            [['tgl_trx', 'tgl_trx_mulai', 'tgl_trx_selesai'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_trx_bellboy' => 'Id Trx Bellboy',
            'id_pegawai' => 'Id Pegawai',
            'tgl_trx' => 'Tgl Trx',
            'status' => 'Status',
            'tgl_trx_mulai' => 'Tgl Trx Mulai',
            'tgl_trx_selesai' => 'Tgl Trx Selesai',
        ];
    }
}
