<?php

namespace common\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\TransaksiKamar;

/**
 * TransaksiKamarSearch represents the model behind the search form of `common\models\TransaksiKamar`.
 */
class TransaksiKamarSearch extends TransaksiKamar
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_transaksi_kamar_detail', 'id_transaksi_kamar_header', 'id_kamar'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = TransaksiKamar::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id_transaksi_kamar_detail' => $this->id_transaksi_kamar_detail,
            'id_transaksi_kamar_header' => $this->id_transaksi_kamar_header,
            'id_kamar' => $this->id_kamar,
        ]);

        return $dataProvider;
    }
}
