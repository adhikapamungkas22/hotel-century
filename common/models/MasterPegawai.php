<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "master_pegawai".
 *
 * @property int $id_pegawai
 * @property string $nip_pegawai
 * @property string $nama_pegawai
 * @property string $alamat_pegawai
 * @property string $email_pegawai
 * @property string $password_pegawai
 * @property int $id_jabatan
 */
class MasterPegawai extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'master_pegawai';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nip_pegawai', 'nama_pegawai', 'alamat_pegawai', 'email_pegawai', 'password_pegawai', 'id_jabatan'], 'required'],
            [['id_jabatan'], 'integer'],
            [['nip_pegawai', 'password_pegawai'], 'string', 'max' => 20],
            [['nama_pegawai', 'alamat_pegawai'], 'string', 'max' => 100],
            [['email_pegawai'], 'string', 'max' => 50],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_pegawai' => 'Id Pegawai',
            'nip_pegawai' => 'Nip Pegawai',
            'nama_pegawai' => 'Nama Pegawai',
            'alamat_pegawai' => 'Alamat Pegawai',
            'email_pegawai' => 'Email Pegawai',
            'password_pegawai' => 'Password Pegawai',
            'id_jabatan' => 'Id Jabatan',
        ];
    }
}
