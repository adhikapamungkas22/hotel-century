<?php

namespace common\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\TransaksiTamu;

/**
 * TransaksiTamuSearch represents the model behind the search form of `common\models\TransaksiTamu`.
 */
class TransaksiTamuSearch extends TransaksiTamu
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_transaksi_kamar_header', 'id_kamar', 'id_customer'], 'integer'],
            [['kode_pesan', 'tanggal_checkin', 'tanggal_checkout'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = TransaksiTamu::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id_transaksi_kamar_header' => $this->id_transaksi_kamar_header,
            'id_kamar' => $this->id_kamar,
            'id_customer' => $this->id_customer,
            'tanggal_checkin' => $this->tanggal_checkin,
            'tanggal_checkout' => $this->tanggal_checkout,
        ]);

        $query->andFilterWhere(['like', 'kode_pesan', $this->kode_pesan]);

        return $dataProvider;
    }
}
