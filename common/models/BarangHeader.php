<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "barang_header".
 *
 * @property int $id_barang_header
 * @property int $id_customer
 * @property string $tanggal
 */
class BarangHeader extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'barang_header';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_customer', 'tanggal'], 'required'],
            [['id_customer'], 'integer'],
            [['tanggal'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_barang_header' => 'Id Barang Header',
            'id_customer' => 'Id Customer',
            'tanggal' => 'Tanggal',
        ];
    }
}
