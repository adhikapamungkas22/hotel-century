<?php

namespace common\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\MasterKamar;

/**
 * MasterKamarSearch represents the model behind the search form of `common\models\MasterKamar`.
 */
class MasterKamarSearch extends MasterKamar
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_kamar'], 'integer'],
            [['kode_kamar', 'type_kamar'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = MasterKamar::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id_kamar' => $this->id_kamar,
        ]);

        $query->andFilterWhere(['like', 'kode_kamar', $this->kode_kamar])
            ->andFilterWhere(['like', 'type_kamar', $this->type_kamar]);

        return $dataProvider;
    }
}
