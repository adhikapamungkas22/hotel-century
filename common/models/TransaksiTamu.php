<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "trx_tamu".
 *
 * @property int $id_transaksi_kamar_header
 * @property int $id_kamar
 * @property int $id_customer
 * @property string $kode_pesan
 * @property string $tanggal_checkin
 * @property string $tanggal_checkout
 */
class TransaksiTamu extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'trx_tamu';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_kamar', 'id_customer', 'kode_pesan', 'tanggal_checkin', 'tanggal_checkout'], 'required'],
            [['id_kamar', 'id_customer'], 'integer'],
            [['tanggal_checkin', 'tanggal_checkout'], 'safe'],
            [['kode_pesan'], 'string', 'max' => 20],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_transaksi_kamar_header' => 'Id Transaksi Kamar Header',
            'id_kamar' => 'Id Kamar',
            'id_customer' => 'Id Customer',
            'kode_pesan' => 'Kode Pesan',
            'tanggal_checkin' => 'Tanggal Checkin',
            'tanggal_checkout' => 'Tanggal Checkout',
        ];
    }
}
