<?php

namespace common\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\BellboyMaster;

/**
 * BellboyMasterSearch represents the model behind the search form of `common\models\BellboyMaster`.
 */
class BellboyMasterSearch extends BellboyMaster
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_bellboy', 'nip'], 'integer'],
            [['nama_bellboy'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = BellboyMaster::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id_bellboy' => $this->id_bellboy,
            'nip' => $this->nip,
        ]);

        $query->andFilterWhere(['like', 'nama_bellboy', $this->nama_bellboy]);

        return $dataProvider;
    }
}
