<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "penilaian".
 *
 * @property int $id_penilaian
 * @property int $id_customer
 * @property string $komentar
 */
class Penilaian extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'penilaian';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_customer', 'komentar'], 'required'],
            [['id_customer'], 'integer'],
            [['komentar'], 'string'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_penilaian' => 'Id Penilaian',
            'id_customer' => 'Id Customer',
            'komentar' => 'Komentar',
        ];
    }
}
