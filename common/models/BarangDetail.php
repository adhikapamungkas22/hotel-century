<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "barang_detail".
 *
 * @property int $id_barang_detail
 * @property string $nama_barang
 * @property int $jumlah_barang
 * @property int $id_barang-header
 */
class BarangDetail extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'barang_detail';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nama_barang', 'jumlah_barang', 'id_barang-header'], 'required'],
            [['jumlah_barang', 'id_barang_header'], 'integer'],
            [['nama_barang'], 'string', 'max' => 50],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_barang_detail' => 'Id Barang Detail',
            'nama_barang' => 'Nama Barang',
            'jumlah_barang' => 'Jumlah Barang',
            'id_barang_header' => 'Id Barang Header',
        ];
    }
}
