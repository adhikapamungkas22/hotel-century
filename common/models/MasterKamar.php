<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "master_kamar".
 *
 * @property int $id_kamar
 * @property string $kode_kamar
 * @property string $type_kamar
 */
class MasterKamar extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'master_kamar';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['kode_kamar', 'type_kamar'], 'required'],
            [['kode_kamar', 'type_kamar'], 'string', 'max' => 20],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_kamar' => 'Id Kamar',
            'kode_kamar' => 'Kode Kamar',
            'type_kamar' => 'Type Kamar',
        ];
    }
}
