<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "transaksi_kamar_detail".
 *
 * @property int $id_transaksi_kamar_detail
 * @property int $id_transaksi_kamar_header
 * @property int $id_kamar
 */
class TransaksiKamar extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'transaksi_kamar_detail';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_transaksi_kamar_header', 'id_kamar'], 'required'],
            [['id_transaksi_kamar_header', 'id_kamar'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_transaksi_kamar_detail' => 'Id Transaksi Kamar Detail',
            'id_transaksi_kamar_header' => 'Id Transaksi Kamar Header',
            'id_kamar' => 'Id Kamar',
        ];
    }
}
