<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "pengantar_barang".
 *
 * @property int $id_pengantar
 * @property int $id_barang-header
 * @property int $id_bellboy
 * @property string $jam_antar
 * @property string $jam_selesai
 */
class PengantarBarang extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'pengantar_barang';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_barang_header', 'id_bellboy', 'jam_antar', 'jam_selesai'], 'required'],
            [['id_barang_header', 'id_bellboy'], 'integer'],
            [['jam_antar', 'jam_selesai'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_pengantar' => 'Id Pengantar',
            'id_barang_header' => 'Id Barang Header',
            'id_bellboy' => 'Id Bellboy',
            'jam_antar' => 'Jam Antar',
            'jam_selesai' => 'Jam Selesai',
        ];
    }
}
