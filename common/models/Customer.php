<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "customer".
 *
 * @property int $id_customer
 * @property string $nama
 * @property string $no_ktp
 * @property string $alamat
 * @property int $no_hp
 */
class Customer extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'customer';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nama', 'no_ktp', 'alamat', 'no_hp'], 'required'],
            [['alamat'], 'string'],
            [['no_hp'], 'string'],
            [['nama'], 'string', 'max' => 100],
            [['no_ktp'], 'string', 'max' => 30],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_customer' => 'Id Customer',
            'nama' => 'Nama',
            'no_ktp' => 'No Ktp',
            'alamat' => 'Alamat',
            'no_hp' => 'No Hp',
        ];
    }
}
