<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\TransaksiKamar */

$this->title = 'Update Transaksi Kamar: ' . $model->id_transaksi_kamar_detail;
$this->params['breadcrumbs'][] = ['label' => 'Transaksi Kamars', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id_transaksi_kamar_detail, 'url' => ['view', 'id' => $model->id_transaksi_kamar_detail]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="transaksi-kamar-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
