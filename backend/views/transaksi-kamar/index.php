<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\TransaksiKamarSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Transaksi Kamar';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="transaksi-kamar-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Transaksi Kamar', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id_transaksi_kamar_detail',
            'id_transaksi_kamar_header',
            'id_kamar',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
