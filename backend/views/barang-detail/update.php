<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\BarangDetail */

$this->title = 'Update Barang Detail: ' . $model->id_barang_detail;
$this->params['breadcrumbs'][] = ['label' => 'Barang Details', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id_barang_detail, 'url' => ['view', 'id' => $model->id_barang_detail]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="barang-detail-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
