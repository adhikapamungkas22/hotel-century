<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\BarangDetailSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="barang-detail-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id_barang_detail') ?>

    <?= $form->field($model, 'nama_barang') ?>

    <?= $form->field($model, 'jumlah_barang') ?>

    <?= $form->field($model, 'id_barang-header') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
