<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\BarangDetailSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Barang Details';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="barang-detail-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Barang Detail', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id_barang_detail',
            'nama_barang',
            'jumlah_barang',
            'id_barang_header',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
