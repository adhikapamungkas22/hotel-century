<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\MasterKamar */

$this->title = 'Update Master Kamar: ' . $model->id_kamar;
$this->params['breadcrumbs'][] = ['label' => 'Master Kamars', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id_kamar, 'url' => ['view', 'id' => $model->id_kamar]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="master-kamar-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
