<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\MasterKamar */

$this->title = 'Create Master Kamar';
$this->params['breadcrumbs'][] = ['label' => 'Master Kamars', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="master-kamar-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
