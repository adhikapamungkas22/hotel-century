<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\MasterKamar */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="master-kamar-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'kode_kamar')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'type_kamar')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
