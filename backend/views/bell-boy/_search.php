<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\TrxBellboySearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="trx-bellboy-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id_trx_bellboy') ?>

    <?= $form->field($model, 'id_pegawai') ?>

    <?= $form->field($model, 'tgl_trx') ?>

    <?= $form->field($model, 'status') ?>

    <?= $form->field($model, 'tgl_trx_mulai') ?>

    <?php // echo $form->field($model, 'tgl_trx_selesai') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
