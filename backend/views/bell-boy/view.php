<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\TrxBellboy */

$this->title = $model->id_trx_bellboy;
$this->params['breadcrumbs'][] = ['label' => 'Trx Bellboys', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="trx-bellboy-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id_trx_bellboy], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id_trx_bellboy], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id_trx_bellboy',
            'id_pegawai',
            'tgl_trx',
            'status',
            'tgl_trx_mulai',
            'tgl_trx_selesai',
        ],
    ]) ?>

</div>
