<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\TrxBellboySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Data Transaksi Bellboy';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="trx-bellboy-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <!-- <p> -->
         <!-- Html::a('Create Trx Bellboy', ['create'], ['class' => 'btn btn-success']) ?> -->
    <!-- </p> -->

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id_trx_bellboy',
            'id_pegawai',
            'tgl_trx',
            'status',
            'tgl_trx_mulai',
            'tgl_trx_selesai',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
