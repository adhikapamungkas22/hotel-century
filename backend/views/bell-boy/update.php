<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\TrxBellboy */

$this->title = 'Update Trx Bellboy: ' . $model->id_trx_bellboy;
$this->params['breadcrumbs'][] = ['label' => 'Trx Bellboys', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id_trx_bellboy, 'url' => ['view', 'id' => $model->id_trx_bellboy]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="trx-bellboy-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
