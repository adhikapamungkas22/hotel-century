<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\TrxBellboy */

$this->title = 'Create Trx Bellboy';
$this->params['breadcrumbs'][] = ['label' => 'Trx Bellboys', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="trx-bellboy-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
