<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Login';
$this->params['breadcrumbs'][] = $this->title;
?>
<style type="text/css">
    .login-page{
        background-color: #fff;
    }
</style>
<div id="login">
<div class="login-logo">
    <!-- <a href="../../index2.html"><b>Hotel Century</b></a> -->
    <?php echo Html::img('@web/logo.jpg',['width' => '200px',

    'height' => '200px']) ?>

</div>
<div class="login-box-body">
    <p class="login-box-msg">Selamat Datang di Century Park Hotel</p>

     <?php $form = ActiveForm::begin(['id' => 'login-form']); ?>
        <div class="form-group has-feedback">
            <?= $form->field($model, 'username')->textInput(['autofocus' => true]) ?>
            <!-- <input type="email" class="form-control" placeholder="Email"> -->
            <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
        </div>
        <div class="form-group has-feedback">
            <?= $form->field($model, 'password')->passwordInput() ?>
            <!-- <input type="password" class="form-control" placeholder="Password"> -->
            <span class="glyphicon glyphicon-lock form-control-feedback"></span>
        </div>
        <div class="row">
            <div class="col-xs-8">
            <div class="checkbox icheck">
            <label>
                <?= $form->field($model, 'rememberMe')->checkbox() ?>
            </label>
            </div>
            </div>
            <!-- /.col -->
            <div class="col-xs-4">
                <div class="form-group">
                    <?= Html::submitButton('Login', ['class' => 'btn btn-primary', 'name' => 'login-button']) ?>
                </div>
            </div>
            <!-- /.col -->
        </div>
    <?php ActiveForm::end(); ?>

    <!-- <a href="#">I forgot my password</a><br>
    <a href="register.html" class="text-center">Register a new membership</a> -->
</div>
  <!-- /.login-box-body -->
</div> 