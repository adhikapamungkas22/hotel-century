<?php
  use yii\helpers\Html;
  use yii\helpers\Url; 
?>
<!-- Left side column. contains the logo and sidebar -->
<!-- sidebar: style can be found in sidebar.less -->
<section class="sidebar">
  <!-- Sidebar user panel -->
  <div class="user-panel">
    <div class="pull-left image">
      <img src="dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">
    </div>
    <div class="pull-left info">
      <p>Admin</p>
      <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
    </div>
  </div>
  <!-- search form -->
  <form action="#" method="get" class="sidebar-form">
    <div class="input-group">
      <input type="text" name="q" class="form-control" placeholder="Search...">
      <span class="input-group-btn">
            <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
            </button>
          </span>
    </div>
  </form>
  <!-- /.search form -->
  <!-- sidebar menu: : style can be found in sidebar.less -->
  <ul class="sidebar-menu" data-widget="tree">
    <!-- <li class="header">MAIN NAVIGATION</li> -->
    <li>
      <a href="<?=Url::to(['site/index'])?>">
        <i class="fa fa-dashboard"></i> <span>Dashboard</span>
      </a>
    </li>
    <li>
      <a href="<?=Url::to(['barang-detail/index'])?>">
        <i class="fa fa-th"></i> <span>Barang detail</span>
      </a>
    </li>
    <li>
      <a href="<?=Url::to(['barang-header/index'])?>">
        <i class="fa fa-table"></i> <span>barang header</span>
      </a>
    </li>
    <li>
      <a href="<?=Url::to(['customer/index'])?>">
        <i class="fa fa-table"></i> <span>Customer</span>
      </a>
    </li>
    <li>
      <a href="<?=Url::to(['pegawai/index'])?>">
        <i class="fa fa-pie-chart"></i>
        <span>Daftar Pegawai</span>
      </a>
    </li>
    <li>
      <a href="<?=Url::to(['pengantar-barang/index'])?>">
        <i class="fa fa-laptop"></i>
        <span>Pengantar Barang</span>
      </a>
    </li>
    <li>
      <a href="<?=Url::to(['penilaian/index'])?>">
        <i class="fa fa-table"></i> <span>Penilaian</span>
      </a>
    </li>
    <li class="treeview">
      <a href="#">
        <i class="fa fa-edit"></i> <span>Transaksi</span>
        <span class="pull-right-container">
          <i class="fa fa-angle-left pull-right"></i>
        </span>
      </a>
      <ul class="treeview-menu">
        <li><a href="<?=Url::to(['transaksi-tamu/index'])?>"><i class="fa fa-circle-o"></i>Transaksi Tamu</a></li>
        <li><a href="<?=Url::to(['transaksi-kamar/index'])?>"><i class="fa fa-circle-o"></i>Transaksi Kamar</a></li>
        <li><a href="<?=Url::to(['bell-boy/index'])?>"><i class="fa fa-circle-o"></i> Transaksi BellBoy</a></li>
      </ul>
    </li>
    <li>
      <a href="<?=Url::to(['bellboy-master/index'])?>">
        <i class="fa fa-th"></i> <span>BellBoy Master</span>
      </a>
    </li>
    <li>
      <a href="<?=Url::to(['master-kamar/index'])?>">
        <i class="fa fa-th"></i> <span>Master Kamar</span>
      </a>
    </li>
    <li>
      <a href="<?=Url::to(['master-jabatan/index'])?>">
        <i class="fa fa-th"></i> <span>Master Jabatan</span>
      </a>
    </li>
  </ul>
</section>
<!-- /.sidebar -->