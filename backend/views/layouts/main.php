<?php

/* @var $this \yii\web\View */
/* @var $content string */

use backend\assets\AppAsset;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use common\widgets\Alert;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body class="hold-transition skin-blue sidebar-mini">
<?php $this->beginBody() ?>

<div class="wrap">
    <header class="main-header">
        <?= $this->render('header')?>
    </header>
    <aside class="main-sidebar">
        <?= $this->render('leftBar')?>
    </aside>
    <div class="content-wrapper">
        <section class="content">
            <!-- page start-->
            <?= Breadcrumbs::widget([
                'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
            ]) ?>
            <?= $content ?>
            <!-- page end-->
        </section>
    </div>
    <footer class="main-footer">
        <?= $this->render('footer')?>
    </footer>
    <aside class="control-sidebar control-sidebar-dark" style="display: none;">
        <?= $this->render('aside')?>
    </aside>
</div>
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
<script>
  $.widget.bridge('uibutton', $.ui.button);
</script>