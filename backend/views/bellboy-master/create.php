<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\BellboyMaster */

$this->title = 'Create Bellboy Master';
$this->params['breadcrumbs'][] = ['label' => 'Bellboy Masters', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="bellboy-master-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
