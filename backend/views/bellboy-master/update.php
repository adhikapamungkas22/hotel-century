<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\BellboyMaster */

$this->title = 'Update Bellboy Master: ' . $model->id_bellboy;
$this->params['breadcrumbs'][] = ['label' => 'Bellboy Masters', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id_bellboy, 'url' => ['view', 'id' => $model->id_bellboy]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="bellboy-master-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
