<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\BarangHeader */

$this->title = 'Update Barang Header: ' . $model->id_barang_header;
$this->params['breadcrumbs'][] = ['label' => 'Barang Headers', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id_barang_header, 'url' => ['view', 'id' => $model->id_barang_header]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="barang-header-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
