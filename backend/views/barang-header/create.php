<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\BarangHeader */

$this->title = 'Create Barang Header';
$this->params['breadcrumbs'][] = ['label' => 'Barang Headers', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="barang-header-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
