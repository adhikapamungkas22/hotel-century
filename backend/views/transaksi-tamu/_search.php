<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\TransaksiTamuSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="transaksi-tamu-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id_transaksi_kamar_header') ?>

    <?= $form->field($model, 'id_kamar') ?>

    <?= $form->field($model, 'id_customer') ?>

    <?= $form->field($model, 'kode_pesan') ?>

    <?= $form->field($model, 'tanggal_checkin') ?>

    <?php // echo $form->field($model, 'tanggal_checkout') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
