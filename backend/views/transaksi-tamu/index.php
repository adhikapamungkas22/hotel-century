<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\TransaksiTamuSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Transaksi Tamu';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="transaksi-tamu-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Transaksi Tamu', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id_transaksi_kamar_header',
            'id_kamar',
            'id_customer',
            'kode_pesan',
            'tanggal_checkin',
            //'tanggal_checkout',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
