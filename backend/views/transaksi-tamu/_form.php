<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\TransaksiTamu */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="transaksi-tamu-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'id_kamar')->textInput() ?>

    <?= $form->field($model, 'id_customer')->textInput() ?>

    <?= $form->field($model, 'kode_pesan')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'tanggal_checkin')->textInput() ?>

    <?= $form->field($model, 'tanggal_checkout')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
