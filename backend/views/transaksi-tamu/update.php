<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\TransaksiTamu */

$this->title = 'Update Transaksi Tamu: ' . $model->id_transaksi_kamar_header;
$this->params['breadcrumbs'][] = ['label' => 'Transaksi Tamus', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id_transaksi_kamar_header, 'url' => ['view', 'id' => $model->id_transaksi_kamar_header]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="transaksi-tamu-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
