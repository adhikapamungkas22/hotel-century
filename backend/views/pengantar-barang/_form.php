<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\PengantarBarang */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="pengantar-barang-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'id_barang-header')->textInput() ?>

    <?= $form->field($model, 'id_bellboy')->textInput() ?>

    <?= $form->field($model, 'jam_antar')->textInput() ?>

    <?= $form->field($model, 'jam_selesai')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
