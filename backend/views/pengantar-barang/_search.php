<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\PengantarBarangSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="pengantar-barang-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id_pengantar') ?>

    <?= $form->field($model, 'id_barang-header') ?>

    <?= $form->field($model, 'id_bellboy') ?>

    <?= $form->field($model, 'jam_antar') ?>

    <?= $form->field($model, 'jam_selesai') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
