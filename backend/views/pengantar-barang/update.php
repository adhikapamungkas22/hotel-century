<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\PengantarBarang */

$this->title = 'Update Pengantar Barang: ' . $model->id_pengantar;
$this->params['breadcrumbs'][] = ['label' => 'Pengantar Barangs', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id_pengantar, 'url' => ['view', 'id' => $model->id_pengantar]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="pengantar-barang-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
