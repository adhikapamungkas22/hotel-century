<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\PengantarBarang */

$this->title = 'Create Pengantar Barang';
$this->params['breadcrumbs'][] = ['label' => 'Pengantar Barangs', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pengantar-barang-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
